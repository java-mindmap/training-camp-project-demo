package hello.mapper;

import org.apache.ibatis.annotations.Param;

import hello.entity.Student;

public interface StudentMapper {

	Student login(@Param("username") String username, @Param("password") String password);

}
