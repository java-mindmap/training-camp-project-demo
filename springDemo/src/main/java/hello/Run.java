package hello;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Run {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		MessageService service = (MessageService) context.getBean("messageService");
		System.out.println(service.getMessage());
		
		MessagePrinter printer = (MessagePrinter) context.getBean("messagePrinter");
		printer.printMessage();
	}
}	
